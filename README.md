# Create composite and calculate NDVI in ArcGIS

# A Jupyter notebook that creates a composite and NDVI layer of species approximate range for species distribution models.

#### This is a project for the USU WILD 6920 Python GIS Project class by Jesse Tabor.

This is a Jupyter notebook that allows a user to efficiently create a composite and NDVI layer, create a convex hull from the localities of the species of interest, then clip the composite and NDVI layer to the convex hull. This is a quick way to decrease the size of input data needed to run a species distribution model. Rather than training the model with areas outside of the native range, this notebook can decrease large datasets and improve workflow and processing times.

## In this example we use localities of Loxioides bailleui (Palila) on Hawai'i Island.
| Palila                           | 
| :------------------------------: | 
| ![original](images/palila_draw.png) |

The Palila is a critically endangered finch-billed species of the Hawaiian honeycreeper. The bird has a close ecological relationship with the mamane tree (Sophora chrysophylla), and became endangered due to the destruction of the trees.

## Final product below:

| Composite clipped to species range                           | NDVI clipped to species range                          |
| :------------------------------: | :----------------------------: |
| ![original](images/after_composite.png) | ![recoded](images/after_NDVI.png) |



### Step 1: Establish data folder with remotely sensed imagery and occurrence points. Then establish variables and import required packages.

To begin, download your favorite remotely sensed data from sources like EarthExplorer.com or Sentinel2. For this example I used Landsat 8 imagery. You can access locality data for many different species from the Global biodiversity Information Facility or GBIF.com. ArcPy requires a Spatial Analyst license. Confirm that you have access to the spatial analyist extention in ArcGIS Pro before you begin processing data. You will need to point to your data folder and .csv file to tell ArcGIS where to look for your data. Next, rename any of the output files to reflect your specific project. You will need to establish variables for composites, NDVI, point shapefiles, convex hull, and buffer.

Import the required packages and establish your workspace. Also, its important to establish your spatial reference (in the example I use WGS 1984. 


### Step 2: Create a composite and NDVI layers

Create a composite from the available bands in your imagery. For this example, I used bands two, three, four, and five. The information you are interested in will determine what bands you include. Next, calculate the NDVI raster. NDVI quantifies vegetation by measuring the difference between near-infrared (which vegetation strongly reflects) and red light (which vegetation absorbs). NDVI is delivered as a single band product. NDVI or the density of vegetation is calculated by the following equation: (NIR - Red) / (NIR + Red). When using  Landsat 8 specifically: NDVI = (Band 5 – Band 4) / (Band 5 + Band 4). The imagery you use may require different bands used in the equation. NDVI always ranges from -1 to 1. Finally, save your NDVI product as using the raster.save function.

| Composite                           | NDVI                          |
| :------------------------------: | :----------------------------: |
| ![original](images/before_composite.png) | ![recoded](images/before_NDVI.png) |

### Step 3: Create convex hull from Species occurrence points

To convert .csv file into points shapefile, we need to first convert it into XY coordinates. You may need to clean up rename your Longitude and Latitude columns in your .csv file. Make sure your column names match the XFieldName and YFieldName in the arcpy.MakeXYEventLayer_management() function. Once you have XY coordinates, transform that into a points shapefile using arcpy.FeatureClassToShapefile_conversion() function. You will use this points shapefile to create the convex hull.

| Original convex hull                           |
| :------------------------------: |
| ![original](images/og_con.png) |

In geometry a convex hull or convex envelope of a shape is the smallest convex set that contains it. In this case it is the smallest convex polygon that can contain our species points. We use minimum bounding geometry around our points to create our convex hull. Since we are using our convex hull to clip a raster, some pixels around the edge of the convex hull may not be caputred in our clipped output raster. See example below:

Example 1 is an image of the southwest corner of the convex hull, pay close attention to the occurrence point at the southwest corner.
Example 2 is an enlarged image of the point at the southwest corner. As you can see we have an occurrence point with no raster data we can associate it with. To correct for this we need to impliment the use of a buffer zone.

| Example 1. Missing data                           | Example 2. Enlarged area of missing data                          |
| :------------------------------: | :----------------------------: |
| ![original](images/ex1.png) | ![recoded](images/ex2.png) |

We use the arcpy.analysis.Buffer() function to create a 0.01 degree (~1km) buffer or radius around our convex hull. By using this technique we can capture that environmental data our species is associated with on the edge of its range. We combine our buffer zone and convex hull into a final shapefile we can use to clip our composite raster and our NDVI raster.

| Convex hull with ~1km buffer zome                          |
| :----------------------------: |
| ![recoded](images/convex_hull.png) |


### Step 4: Clip composite and NDVI layer to convex hull to represent native range

Next we clip the composite raster and NDVI raster to the convex hull which represents our species range. Rather than clipping the raster to the extent of the bounding box that our convex hull occupies, we choose to maintain the extent of the clipped raster specifically to the shape of our convex hull.


## Congratulations! Now you have an approximate species range you can use to measure predict habitat suitability. This method can be applied to other raster data that can be a predictor in species distributions. Other variables include temperature, precipitation, elevation, slope, aspect, and terain ruggedness.

| Composite clipped to species range                           | NDVI clipped to species range                          |
| :------------------------------: | :----------------------------: |
| ![original](images/after_composite.png) | ![recoded](images/after_NDVI.png) |



