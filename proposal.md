# Project Proposal
## Create a composite raster and Calculate NDVI from remotely sensed imagery

Sometimes it is useful to create a composite raster and calculate NDVI from remotely sensed data. When it is necessary to perform this task on multiple images, it is advantageous to automate the process to work more efficiently. I propose to create a python script for ArcGIS Pro, that allows a user to create a composite raster from satellite imagery, clip the composite to an area of interest based on a convex hull made from species ocurrence points, and calculate NDVI to determine vegetation health in the area of interest.

## Background
Species distribution models have become very popular to predict suitable habitat for a species across space and time. The resulting NDVI layer of the species extent can be used as a parameter to create a species distribution model and estimate habitat suitability within the habitat range of the target species.

Typically, raster data are very large and has long processing times. By creating a minimum convex polygon around the species ocurrence points we can speed up processing times by only analyzing the specific area of interest. When creating multiple species distribution models this greatly can improve processing times.

## Data and parameters
The required data will be 1) species occurence data downloaded from gbif.com and 2) remote sensing data downloaded from earthexplorer.com. The resulting output datasets from the resaerch will be a 1) minimum convex polygon shapefile of the habitat extent and a 2) NDVI raster layer of the area of interest.

## Potential pitfalls
1. If multiple images are needed it will be difficult to find imagery of the habitat extent captured on the same date. If imagery captured on different dates is used this will affect the results.

2. If multiple images need to be used it may be necessary to resample the rasters to fit correctly with eachother. Also, I dont know how to create an orthomosaic or merge rasters in Python. 

3. Not all imagery has the spectral data needed to calculate NDVI.

4. If remotely sensed raster is clipped from minimum convex polygon based on ocurrence points, some points may not be associated with any pixel in the clipped raster. 

## Solutions
1. I will aim to find imagery taken on the same date or find a species who's entire habitat fits within one image. 

3. Imagery will be chosen based on if it includes the spectral bands required for NDVI. 

4. Buffer zone may need to be created around the minimum convex polygon to include habitat of ocurrence points on the boarder of the polygon.
